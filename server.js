const express = require('express')
const dotenv = require('dotenv')
const app = express()
const mongoose = require('mongoose');
const CategoryRoute = require("./routers/category");
const RestaurantRoute = require("./routers/restaurant");
const FoodRoute = require("./routers/food");

dotenv.config();

mongoose.connect(process.env.MONGOURL)
.then(() => console.log("Foodly Database Connecting !!"))
.catch((err) => console.log(err));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/api/category", CategoryRoute);
app.use("/api/restaurant", RestaurantRoute);
app.use("/api/foods", FoodRoute);

app.listen(process.env.PORT|| 6013, () => console.log(`Backend is running on ${process.env.PORT}!`))